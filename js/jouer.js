chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }

  document.body.id = "jouer";
  
  // On récupère les différents tableaux
  let tables = document.querySelectorAll("table");
  let hrpMenu = tables[0];  // Contient l'heure serveur, le lien de déconnexion...
  let infoTable = tables[1]; // Contient le grade du perso, le bataillon, la compagnie...
  let rpMenu = tables[2]; // Contient les liens vers d'autres pages de jeu...
  let gameBody = getGameBody(tables[3]); // Contient le corps du jeu, la carte, les actions...
  let combatTable = gameBody.querySelector("form[action='agir.php']").parentElement.parentElement.parentElement;
  let moveTableContainer = gameBody.rows[0].cells[0].lastElementChild;
  let moveTable = moveTableContainer.querySelector("table");
  let visuTableContainer = gameBody.rows[0].cells[1].firstElementChild;
  let visuTable = visuTableContainer.querySelector("table");
  let actionsTableContainer = gameBody.rows[0].cells[2].firstElementChild;
  let actionsTable = actionsTableContainer.querySelector("table");

  // On supprime les images inutiles
  hrpMenu.querySelectorAll("img").forEach((img) => { img.remove(); }); // Horloge, Bandeau central
  rpMenu.querySelectorAll("tr")[0].remove(); // Illustrations du menu

  // Relifting du menu hrp
  hrpMenu.setAttribute("id", "hrpMenu");
  hrpMenu.querySelectorAll("a").forEach((a) => { a.removeAttribute("class") });

  // Relifting du tableau d'informations
  infoTable.setAttribute("id", "infoTable");
  infoTable.querySelector("input[type=submit]").remove();

  // Déplacement du lien de rafraichissement de la page
  let pageRefreshLinkTableRow = rpMenu.querySelectorAll("tr")[2];
  let pageRefreshLinkHTML = pageRefreshLinkTableRow.querySelector("td").innerHTML;
  hrpMenu.querySelector("td[rowspan]").innerHTML = pageRefreshLinkHTML;
  pageRefreshLinkTableRow.remove();
  document.querySelector("body > br").remove();

  // Retouches Game Body
  gameBody.setAttribute("id", "gameBody");
  gameBody.setAttribute("cellpadding", "0");
  
  
  // Relifting du tableau de combat
  combatTable.setAttribute("id", "combatTable");
  combatTable.removeAttribute("style");
  combatTable.querySelector("td[colspan]").bgColor="#CCC";
  combatTable.querySelector("input[type='submit']").value="ATTAQUE";
  
  // Relifting du tableau de déplacement
  moveTableContainer.setAttribute("id", "moveTableContainer");
  moveTable.setAttribute("id", "moveTable");

  moveTableContainer.removeAttribute("border");
  moveTableContainer.querySelector("td").removeAttribute("background");
  moveTable.setAttribute("align", "right");
  moveTable.rows[0].cells[0].setAttribute("colspan","3");
  moveTable.rows[0].cells[0].innerText="Se déplacer";
  moveTable.rows[1].querySelectorAll("td[rowspan]").forEach( (td) => { td.remove() });
  moveTable.removeAttribute("border");
  
  // Relifting de la visu
  visuTableContainer.setAttribute("id", "visuTableContainer");
  visuTable.setAttribute("id", "visuTable");

  visuTable.querySelectorAll("th").forEach( (th) => {
      if (th.attributes.background.value.includes("background3.jpg")) {
          th.setAttribute("data-center", "yes");
      }
  });
  Array.from(visuTable.rows[0].cells).forEach( (cell) => {
      cell.removeAttribute("background");
      cell.removeAttribute("height"); 
  });
  visuTable.removeAttribute("height");
  visuTable.removeAttribute("width");
  visuTable.removeAttribute("border");
  visuTable.removeAttribute("cellspacing");
  visuTable.removeAttribute("cellpadding");
  visuTable.removeAttribute("style:no-padding");
  visuTableContainer.removeAttribute("style");

  // Relifting du tableau d'actions
  actionsTableContainer.setAttribute("id", "actionsTableContainer");
  actionsTableContainer.removeAttribute("style");
  actionsTable.setAttribute("id", "actionsTable");
  actionsTable.removeAttribute("style:no-padding");
  
  actionsTable.querySelectorAll("td[background]").forEach( (td) => { td.removeAttribute("background") });
  actionsTable.rows[0].querySelector("img").remove();
  let actionsSelect = actionsTable.rows[0].querySelector("select");
  actionsSelect.setAttribute("size",actionsSelect.length);
  actionsTable.rows[0].querySelector("input[type=submit]").setAttribute("class", "bouton_connexion");
  actionsTable.rows[1].querySelector("img").remove();
  let eventFormTable = actionsTable.rows[2].querySelector("table");
  eventFormTable.querySelector("td").innerText="Id : ";
  eventFormTable.querySelector("input[type=text]").removeAttribute("style");
  eventFormTable.querySelector("input[type=text]").setAttribute("class", "input_connexion");
  eventFormTable.querySelector("input[type=submit]").setAttribute("class", "bouton_connexion");

  actionsTable.rows[3].querySelectorAll("a").forEach( (a) => {
      let title = a.querySelector("img").getAttribute("data-original-title");
      a.setAttribute("title", title);
      a.querySelector("img").remove();
      a.innerHTML= a.innerText;
  });
  actionsTable.rows[4].querySelector("img").remove();

  /*
    Handle case where there is the list of objects displayed above the gamebody
  */
  function getGameBody(table3) {
    if(table3.previousElementSibling.innerText.includes("Liste")){
      return table3.parentElement.nextElementSibling;
    } else {
      return table3;
    }
  }
});