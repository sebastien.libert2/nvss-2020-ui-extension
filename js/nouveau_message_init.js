var baseurl = document.querySelector("script[data-baseurl]").getAttribute("data-baseurl");

sceditor.create(document.getElementById("textareaMessageImput"), {
  format: 'bbcode',
  locale: 'fr',
  style: baseurl,
  toolbar: "cut,copy,paste,pastetext|bold,italic,underline|color|center|removeformat|image|table|link,unlink|date,time|maximize,source",
  startInSourceMode: true,
  emoticonsEnabled: false,
  autofocusEnd: false,
  plugins: 'undo',
  width: 'auto',
  resizeMaxWidth: -1,
  resizeMaxHeigth: -1
});
